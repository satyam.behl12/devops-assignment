# devops-assignment

## Description

This project creates the resources in Azure cloud using Terraform. The resources created:
 - storage account
 - resource group 


## CI/CD Setup

The .gitlab-ci.yml is configured to run:
 - terraform init
 - terraform validate
 - terraform plan
 - terraform apply

The commands are written in a way to allow Terraform to refresh its state. The state is maintained at a remote Azure backend to ensure reliability.

In order to access the Azure cloud, Terraform needs to be authenticated.
For this, Managed Service Identities authentication of Azure is used. This is achieved by setting up gitlab-runner using docker executor to run on a Virtual Machine (VM) in Azure cloud. This VM is given Contributor privileges to be able to read/list/deploy and resources.  