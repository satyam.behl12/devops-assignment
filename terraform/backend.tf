terraform {
  backend "azurerm" {
    storage_account_name = "saltoksterraform"
    resource_group_name  = "salto-ks-devops"
    container_name       = "terraform"
    use_msi              = true
    key                  = "bootstrap.tfstate"
  }
  
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=2.46.0"
    }
  }
}

provider "azurerm" {
  features {}

  use_msi  = true
}
