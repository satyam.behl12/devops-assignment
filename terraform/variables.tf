variable location {
  type    = string
  default = "westeurope"
}

variable resource_group_name {
  type    = string
  default = "saltoks-devops-test"
}

variable storage_account_name {
  type    = string
  default = "claydevopstest"
}

variable storage_account_tier {
  type    = string
  default = "Standard"
}

variable storage_account_replication_type {
  type    = string
  default = "GRS"
}