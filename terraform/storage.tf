resource "random_string" "storageaccount" {
  length  = 6
  special = false
  upper   = false
}

resource "azurerm_storage_account" "default" {
  name                     = "${var.storage_account_name}${random_string.storageaccount.result}"
  resource_group_name      = azurerm_resource_group.resourcegroup.name
  location                 = azurerm_resource_group.resourcegroup.location
  account_tier             = var.storage_account_tier
  account_replication_type = var.storage_account_replication_type
}
